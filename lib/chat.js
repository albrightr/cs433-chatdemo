var socketio = require('socket.io');
var io;
var nickNames = {};

exports.listen = function(server) {
    io = socketio.listen(server);
    io.set("transports", ["websocket"]);
    
    io.sockets.on('connection', function (socket) {
        socket.emit('message', {text: 'Client connected'});

        socket.on('message', function (message) {
            io.sockets.emit('message', {text: message.text});
        });
    });

};

#!/bin/env node
var http    = require('http');
var fs      = require('fs');
var path    = require('path');
var mime    = require('mime');

// 0.6 compatability
fs.exists = fs.exists || require('path').exists;

var SampleApp = function() {

    //  Scope.
    var self = this;


    /*  ================================================================  */
    /*  Helper functions.                                                 */
    /*  ================================================================  */

    /**
     *  Set up server IP address and port # using env variables/defaults.
     */
    self.setupVariables = function() {
        //  Set the environment variables we need.
        self.ipaddress = process.env.OPENSHIFT_INTERNAL_IP;
        self.port      = process.env.OPENSHIFT_INTERNAL_PORT || 1337;

        if (typeof self.ipaddress === "undefined") {
            //  Log errors on OpenShift but continue w/ 127.0.0.1 - this
            //  allows us to run/test the app locally.
            console.warn('No OPENSHIFT_INTERNAL_IP var');
            self.ipaddress = "";
        };
    };


    self.send404 = function (response) {
        response.writeHead(404, {'Content-Type': 'text/plain'});
        response.write('Error 404: resource not found.');
        response.end();
    }

    self.sendFile = function(response, filePath, fileContents) {
        response.writeHead(
            200,
            {"content-type": mime.lookup(path.basename(filePath))}
        );
        response.end(fileContents);
    }

    self.serveStatic = function(response, absPath) {
        fs.exists(absPath, function (exists) {
            if (exists) {
                fs.readFile(absPath, function(err, data) {
                    if (err) {
                        self.send404(response);
                    } else {
                        self.sendFile(response, absPath, data);
                    }
                });
            } else {
                self.send404(response);	
            }	
        });
    }


    /**
     *  Initialize the server (express) and create the routes and register
     *  the handlers.
     */
    self.initializeServer = function() {
        self.app = http.createServer(function(request, response) {
            var filePath = false;
            
            // Routing
            if (request.url == '/') {
                filePath = 'pub/index.html';
            } else {
                filePath = 'pub' + request.url;
            }

            var absPath = './' + filePath;
            self.serveStatic(response, absPath);
        });
    };

    self.initializeChatSocket = function() {
        var chatServer = require('./lib/chat.js');
        chatServer.listen(self.app);
    };

    /**
     *  Initializes the sample application.
     */
    self.initialize = function() {
        self.setupVariables();

        // Create the express server and routes.
        self.initializeServer();
        self.initializeChatSocket();
    };


    /**
     *  Start the server (starts up the sample application).
     */
    self.start = function() {
        //  Start the app on the specific interface (and port).
        self.app.listen(self.port, self.ipaddress, function() {
            console.log('%s: Node server started on %s:%d ...',
                        Date(Date.now() ), self.ipaddress, self.port);
        });
    };

}; 



/**
 *  main():  Main code.
 */
var zapp = new SampleApp();
zapp.initialize();
zapp.start();

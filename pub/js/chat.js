var Chat = function(socket) {
    this.socket = socket;
};

Chat.prototype.sendMessage = function(message) {
    this.socket.emit('message', {text: message});
};

Chat.prototype.recvMessage = function(message) {
    $('.msg').last().after('<div class="msg"></div>').text(message.text);
    $('#chat').animate({scrollTop: $('#chat').prop("scrollHeight")}, 500);
};

window.onload = function() {
    if (document.URL.indexOf('localhost') !== -1)
        var iosocket = io.connect();
    else
        var iosocket = io.connect('http://chatdemo-defreez.rhcloud.com:8000');

    var chat = new Chat(iosocket);

    iosocket.on('message', function(message) {
        chat.recvMessage(message);
    });

    $('#chatform').submit(function() {
        var user = $('#chat-username').val();
        var message = $('#chat-message').val();
        chat.sendMessage(user + ': ' + message);
        return false;
    });
};
